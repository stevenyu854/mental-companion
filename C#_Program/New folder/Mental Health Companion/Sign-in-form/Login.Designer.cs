﻿namespace Sign_in_form
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtusernamelogin = new System.Windows.Forms.Label();
            this.txtpasswordlogin = new System.Windows.Forms.Label();
            this.loginbttn = new System.Windows.Forms.Button();
            this.registernow = new System.Windows.Forms.Button();
            this.donthaveaccount = new System.Windows.Forms.Label();
            this.txtusernamel = new System.Windows.Forms.TextBox();
            this.txtpasswordl = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtusernamelogin
            // 
            this.txtusernamelogin.AutoSize = true;
            this.txtusernamelogin.Location = new System.Drawing.Point(12, 27);
            this.txtusernamelogin.Name = "txtusernamelogin";
            this.txtusernamelogin.Size = new System.Drawing.Size(73, 17);
            this.txtusernamelogin.TabIndex = 0;
            this.txtusernamelogin.Text = "Username";
            // 
            // txtpasswordlogin
            // 
            this.txtpasswordlogin.AutoSize = true;
            this.txtpasswordlogin.Location = new System.Drawing.Point(12, 87);
            this.txtpasswordlogin.Name = "txtpasswordlogin";
            this.txtpasswordlogin.Size = new System.Drawing.Size(69, 17);
            this.txtpasswordlogin.TabIndex = 1;
            this.txtpasswordlogin.Text = "Password";
            // 
            // loginbttn
            // 
            this.loginbttn.Location = new System.Drawing.Point(164, 152);
            this.loginbttn.Name = "loginbttn";
            this.loginbttn.Size = new System.Drawing.Size(199, 78);
            this.loginbttn.TabIndex = 2;
            this.loginbttn.Text = "Login";
            this.loginbttn.UseVisualStyleBackColor = true;
            this.loginbttn.Click += new System.EventHandler(this.loginbttn_Click);
            // 
            // registernow
            // 
            this.registernow.Location = new System.Drawing.Point(164, 273);
            this.registernow.Name = "registernow";
            this.registernow.Size = new System.Drawing.Size(199, 78);
            this.registernow.TabIndex = 3;
            this.registernow.Text = "Register";
            this.registernow.UseVisualStyleBackColor = true;
            this.registernow.Click += new System.EventHandler(this.registernow_Click);
            // 
            // donthaveaccount
            // 
            this.donthaveaccount.AutoSize = true;
            this.donthaveaccount.Location = new System.Drawing.Point(269, 253);
            this.donthaveaccount.Name = "donthaveaccount";
            this.donthaveaccount.Size = new System.Drawing.Size(158, 17);
            this.donthaveaccount.TabIndex = 4;
            this.donthaveaccount.Text = "Don\'t have an account?";
            // 
            // txtusernamel
            // 
            this.txtusernamel.Location = new System.Drawing.Point(150, 27);
            this.txtusernamel.Name = "txtusernamel";
            this.txtusernamel.Size = new System.Drawing.Size(375, 22);
            this.txtusernamel.TabIndex = 5;
            // 
            // txtpasswordl
            // 
            this.txtpasswordl.Location = new System.Drawing.Point(150, 87);
            this.txtpasswordl.Name = "txtpasswordl";
            this.txtpasswordl.Size = new System.Drawing.Size(375, 22);
            this.txtpasswordl.TabIndex = 6;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 377);
            this.Controls.Add(this.txtpasswordl);
            this.Controls.Add(this.txtusernamel);
            this.Controls.Add(this.donthaveaccount);
            this.Controls.Add(this.registernow);
            this.Controls.Add(this.loginbttn);
            this.Controls.Add(this.txtpasswordlogin);
            this.Controls.Add(this.txtusernamelogin);
            this.Name = "Login";
            this.Text = "Log in";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtusernamelogin;
        private System.Windows.Forms.Label txtpasswordlogin;
        private System.Windows.Forms.Button loginbttn;
        private System.Windows.Forms.Button registernow;
        private System.Windows.Forms.Label donthaveaccount;
        private System.Windows.Forms.TextBox txtusernamel;
        private System.Windows.Forms.TextBox txtpasswordl;
    }
}