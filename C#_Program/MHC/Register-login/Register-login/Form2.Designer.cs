﻿namespace Register_login
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernamelogin = new System.Windows.Forms.Label();
            this.passwordlogin = new System.Windows.Forms.Label();
            this.login_bttn2 = new System.Windows.Forms.Button();
            this.register_bttn2 = new System.Windows.Forms.Button();
            this.haveanaccount = new System.Windows.Forms.Label();
            this.txtusernamel = new System.Windows.Forms.TextBox();
            this.txtpasswordl = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // usernamelogin
            // 
            this.usernamelogin.AutoSize = true;
            this.usernamelogin.Location = new System.Drawing.Point(12, 27);
            this.usernamelogin.Name = "usernamelogin";
            this.usernamelogin.Size = new System.Drawing.Size(73, 17);
            this.usernamelogin.TabIndex = 0;
            this.usernamelogin.Text = "Username";
            // 
            // passwordlogin
            // 
            this.passwordlogin.AutoSize = true;
            this.passwordlogin.Location = new System.Drawing.Point(12, 82);
            this.passwordlogin.Name = "passwordlogin";
            this.passwordlogin.Size = new System.Drawing.Size(69, 17);
            this.passwordlogin.TabIndex = 1;
            this.passwordlogin.Text = "Password";
            // 
            // login_bttn2
            // 
            this.login_bttn2.Location = new System.Drawing.Point(56, 148);
            this.login_bttn2.Name = "login_bttn2";
            this.login_bttn2.Size = new System.Drawing.Size(173, 53);
            this.login_bttn2.TabIndex = 2;
            this.login_bttn2.Text = "Log in";
            this.login_bttn2.UseVisualStyleBackColor = true;
            this.login_bttn2.Click += new System.EventHandler(this.login_bttn2_Click);
            // 
            // register_bttn2
            // 
            this.register_bttn2.Location = new System.Drawing.Point(332, 148);
            this.register_bttn2.Name = "register_bttn2";
            this.register_bttn2.Size = new System.Drawing.Size(176, 53);
            this.register_bttn2.TabIndex = 3;
            this.register_bttn2.Text = "Register";
            this.register_bttn2.UseVisualStyleBackColor = true;
            this.register_bttn2.Click += new System.EventHandler(this.register_bttn2_Click);
            // 
            // haveanaccount
            // 
            this.haveanaccount.AutoSize = true;
            this.haveanaccount.Location = new System.Drawing.Point(350, 128);
            this.haveanaccount.Name = "haveanaccount";
            this.haveanaccount.Size = new System.Drawing.Size(158, 17);
            this.haveanaccount.TabIndex = 4;
            this.haveanaccount.Text = "Don\'t have an account?";
            // 
            // txtusernamel
            // 
            this.txtusernamel.Location = new System.Drawing.Point(135, 24);
            this.txtusernamel.Name = "txtusernamel";
            this.txtusernamel.Size = new System.Drawing.Size(402, 22);
            this.txtusernamel.TabIndex = 5;
            // 
            // txtpasswordl
            // 
            this.txtpasswordl.Location = new System.Drawing.Point(135, 79);
            this.txtpasswordl.Name = "txtpasswordl";
            this.txtpasswordl.PasswordChar = '*';
            this.txtpasswordl.Size = new System.Drawing.Size(402, 22);
            this.txtpasswordl.TabIndex = 6;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 253);
            this.Controls.Add(this.txtpasswordl);
            this.Controls.Add(this.txtusernamel);
            this.Controls.Add(this.haveanaccount);
            this.Controls.Add(this.register_bttn2);
            this.Controls.Add(this.login_bttn2);
            this.Controls.Add(this.passwordlogin);
            this.Controls.Add(this.usernamelogin);
            this.Name = "Form2";
            this.Text = "Log In";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernamelogin;
        private System.Windows.Forms.Label passwordlogin;
        private System.Windows.Forms.Button login_bttn2;
        private System.Windows.Forms.Button register_bttn2;
        private System.Windows.Forms.Label haveanaccount;
        private System.Windows.Forms.TextBox txtusernamel;
        private System.Windows.Forms.TextBox txtpasswordl;
    }
}