﻿namespace Register_login
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usernameregister = new System.Windows.Forms.Label();
            this.passwordregister = new System.Windows.Forms.Label();
            this.confirmpasswordregister = new System.Windows.Forms.Label();
            this.register_bttn1 = new System.Windows.Forms.Button();
            this.login_bttn1 = new System.Windows.Forms.Button();
            this.registeredornot = new System.Windows.Forms.Label();
            this.txtconfirmpassr = new System.Windows.Forms.TextBox();
            this.txtpasswordr = new System.Windows.Forms.TextBox();
            this.txtusernamer = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // usernameregister
            // 
            this.usernameregister.AutoSize = true;
            this.usernameregister.Location = new System.Drawing.Point(12, 31);
            this.usernameregister.Name = "usernameregister";
            this.usernameregister.Size = new System.Drawing.Size(73, 17);
            this.usernameregister.TabIndex = 0;
            this.usernameregister.Text = "Username";
            this.usernameregister.Click += new System.EventHandler(this.usernameregister_Click);
            // 
            // passwordregister
            // 
            this.passwordregister.AutoSize = true;
            this.passwordregister.Location = new System.Drawing.Point(12, 79);
            this.passwordregister.Name = "passwordregister";
            this.passwordregister.Size = new System.Drawing.Size(69, 17);
            this.passwordregister.TabIndex = 1;
            this.passwordregister.Text = "Password";
            // 
            // confirmpasswordregister
            // 
            this.confirmpasswordregister.AutoSize = true;
            this.confirmpasswordregister.Location = new System.Drawing.Point(12, 136);
            this.confirmpasswordregister.Name = "confirmpasswordregister";
            this.confirmpasswordregister.Size = new System.Drawing.Size(121, 17);
            this.confirmpasswordregister.TabIndex = 2;
            this.confirmpasswordregister.Text = "Confirm Password";
            // 
            // register_bttn1
            // 
            this.register_bttn1.Location = new System.Drawing.Point(50, 209);
            this.register_bttn1.Name = "register_bttn1";
            this.register_bttn1.Size = new System.Drawing.Size(182, 52);
            this.register_bttn1.TabIndex = 3;
            this.register_bttn1.Text = "Register";
            this.register_bttn1.UseVisualStyleBackColor = true;
            this.register_bttn1.Click += new System.EventHandler(this.register_bttn1_Click);
            // 
            // login_bttn1
            // 
            this.login_bttn1.Location = new System.Drawing.Point(322, 209);
            this.login_bttn1.Name = "login_bttn1";
            this.login_bttn1.Size = new System.Drawing.Size(181, 52);
            this.login_bttn1.TabIndex = 4;
            this.login_bttn1.Text = "Login";
            this.login_bttn1.UseVisualStyleBackColor = true;
            this.login_bttn1.Click += new System.EventHandler(this.login_bttn1_Click);
            // 
            // registeredornot
            // 
            this.registeredornot.AutoSize = true;
            this.registeredornot.Location = new System.Drawing.Point(366, 189);
            this.registeredornot.Name = "registeredornot";
            this.registeredornot.Size = new System.Drawing.Size(137, 17);
            this.registeredornot.TabIndex = 5;
            this.registeredornot.Text = "Already Registered?";
            this.registeredornot.Click += new System.EventHandler(this.Registeredornot_Click);
            // 
            // txtconfirmpassr
            // 
            this.txtconfirmpassr.Location = new System.Drawing.Point(186, 133);
            this.txtconfirmpassr.Name = "txtconfirmpassr";
            this.txtconfirmpassr.PasswordChar = '*';
            this.txtconfirmpassr.Size = new System.Drawing.Size(332, 22);
            this.txtconfirmpassr.TabIndex = 6;
            // 
            // txtpasswordr
            // 
            this.txtpasswordr.Location = new System.Drawing.Point(186, 76);
            this.txtpasswordr.Name = "txtpasswordr";
            this.txtpasswordr.PasswordChar = '*';
            this.txtpasswordr.Size = new System.Drawing.Size(332, 22);
            this.txtpasswordr.TabIndex = 7;
            // 
            // txtusernamer
            // 
            this.txtusernamer.Location = new System.Drawing.Point(186, 28);
            this.txtusernamer.Name = "txtusernamer";
            this.txtusernamer.Size = new System.Drawing.Size(332, 22);
            this.txtusernamer.TabIndex = 8;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 313);
            this.Controls.Add(this.txtusernamer);
            this.Controls.Add(this.txtpasswordr);
            this.Controls.Add(this.txtconfirmpassr);
            this.Controls.Add(this.registeredornot);
            this.Controls.Add(this.login_bttn1);
            this.Controls.Add(this.register_bttn1);
            this.Controls.Add(this.confirmpasswordregister);
            this.Controls.Add(this.passwordregister);
            this.Controls.Add(this.usernameregister);
            this.Name = "Register";
            this.Text = "Register";
            this.Load += new System.EventHandler(this.Register_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label usernameregister;
        private System.Windows.Forms.Label passwordregister;
        private System.Windows.Forms.Label confirmpasswordregister;
        private System.Windows.Forms.Button register_bttn1;
        private System.Windows.Forms.Button login_bttn1;
        private System.Windows.Forms.Label registeredornot;
        private System.Windows.Forms.TextBox txtconfirmpassr;
        private System.Windows.Forms.TextBox txtpasswordr;
        private System.Windows.Forms.TextBox txtusernamer;
    }
}

