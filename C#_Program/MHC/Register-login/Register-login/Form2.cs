﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Register_login
{
    public partial class Form2 : Form
    {
        private SqlConnection cn;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename= D:\Mikro skill\S7\Repository\C#_Program\MHC\Register-login\Register-login\Database.mdf;Integrated Security=True");
            cn.Open();
        }

        private void register_bttn2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Register registration = new Register();
            registration.ShowDialog();
        }

        private void login_bttn2_Click(object sender, EventArgs e)
        {
            if (txtpasswordl.Text != string.Empty || txtusernamel.Text != string.Empty)
            {
                SqlCommand cmd = new SqlCommand("select * from LoginTable where username='" + txtusernamel.Text + "'and password='" + txtpasswordl.Text + "'", cn);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    this.Hide();
                    Form2 home = new Form2();
                    home.ShowDialog();
                }
                else
                {
                    dr.Close();
                    MessageBox.Show("No account available with this username and password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please enter value in all field", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
